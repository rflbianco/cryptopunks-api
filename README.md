# CryptoPunks API

This project provides a  REST API to [CryptoPunks][crypto-punks] collectible characters on the [Ethereum Blockchain][ethereum].

## Using

This project provides only the following endpoints:
- `GET /crypto-punks/` List of all the punks that are listed for sale.
- `GET /crypto-punks/{punkIndex}` Information and **for-sale price** about a single punk, given its numeric
  identifier.

To access the API, you only need to use any HTTP client, like `cURL` or [Postman][].

After running the application either through [command line](#from-command-line) or [your IDE](#from-your-ide) you can access it on `localhost:8080`.

For example, the list of CryptoPunks indexes available for-sale can be got running the following in your terminal.

```shell script
$ curl http://localhost:8080/crypto-punks
```

It shall return something like:
```json
[510,641,861,950,998,1000,1022,1030,1036,1048,1052,1068,1071,1072,1092,1094,1146,1156,1215,1225,1234,1238,1330,1335,1338,1351,1360,1398,1407,1417,1425,1435,1437,1438,1456,1469,1509,1512,1537,1565,1581,1605,1617,1648,1650,1665,1683,1702,1743,1757,1760,1782,1784,1797,1825,1833,1841,1846,1859,1864,1875,1881,1891,1904,1934,1944,2010,2017,2032,2043,2044,2045,2054,2057,2063,2069,2121,2125,2128,2129,2136,2151,2153,2154,2165,2177,2181,2188,2194,2202,2209,2223,2227,2228,2230,2235,2241,2245,2256,2259,2290,2292,2303,2309,2315,2320,2340,2351,2365,2377,2397,2404,2406,2410,2420,2423,2429,2433,2442,2444,2451,2461,2469,2470,2471,2476,2477,2483,2490,2529,2538,2556,2557,2579,2580,2581,2584,2589,2603,2604,2607,2624,2629,2637,2660,2661,2662,2667,2668,2669,2672,2687,2729,2733,2739,2744,2752,2753,2762,2765,2771,2787,2804,2819,2840,2846,2854,2855,2856,2863,2868,2870,2873,2877,2879,2881,2898,2914,2921,2929,2931,2933,2934,2939,2968,2969,2982,2984,2998,2999,3010,3044,3048,3067,3075,3092,3099,3104,3106,3141,3201,3207,3215,3223,3242,3247,3254,3256,3257,3261,3262,3269,3280,3295,3306,3310,3321,3325,3326,3330,3332,3338,3342,3346,3348,3355,3357,3361,3372,3381,3383,3384,3387,3397,3417,3431,3444,3449,3453,3465,3470,3471,3530,3538,3539,3543,3549,3578,3583,3588,3591,3601,3615,3631,3653,3658,3665,3666,3668,3671,3672,3693,3711,3722,3751,3753,3766,3771,3780,3782,3787,3798,3799,3804,3806,3823,3830,3832,3841,3844,3854,3857,3891,3894,3897,3914,3924,3926,3927,4046,4052,4053,4055,4102,4108,4110,4111,4117,4125,4130,4140,4146,4148,4155,4166,4176,4208,4225,4244,4253,4256,4261,4262,4278,4310,4312,4318,4395,4405,4420,4452,4488,4491,4528,4537,4539,4540,4544,4552,4637,4805,8503,9228,9233]
```

To get the details of a given CryptoPunk, you can run:
```shell script
$ curl http://localhost:8080/crypto-punks/8852
```

and you will get something like:
```json
{"index":8852,"gender":"Male","accessories":["Stringy Hair","Normal Beard Black","Horned Rim Glasses"],"minValue":1800000000000000000} 
```


### Known problems

This project is incomplete and even though functional, it has some well known issues.

- Crypto punks with index over 8999 are not currently returning correctly thanks to missing data in [reference dataset][crypto-punks-details], hence missing data in [CryptoPunks.json](src/main/resources/CryptoPunks.json) 
- The cache of CryptoPunks available for-sale is only loaded on application warm-up, making it prone to outdated data. Among other options, a timer to refresh it might be added or a connection to listen to **offer** and **withdraw** events could be used to incrementally update the cache only when needed.
- Concurrent requests to create the **for-sale cache** may lead to some failures caused by Infura's API limit rate. Such request's failures have not been correctly treated/retried, leading to incomplete list of available CryptoPunks.

### API Documentation

A OpenAPI-based specification is provided as the API documentation.

// TODO add link to swagger live doc or option to it.

## Contributing

### Dependencies/Tooling

This project is JVM-based. It uses to following stack and therefore you must have this tools installed to build and run it.
- JDK 11
- Gradle 5.x

> **Notes:**
>- Currently it is possible to compile and run this project with JDK 8. However it is officially targeted to JDK 11+ and may stop working with older versions in the future.
>- It is only compatible with Gradle 5.x, currently the newest Major release. Using previous versions may fail to build due to breaking changes in library APIs and given script compiling errors when running any task. Please check using `gradle --version`.

### Building and Running

This project uses [Gradle][] as its build tool. If you are not used to this too, below there is a quick guide of the main tasks available. For a more comprehensive understanding, the [tool's documentation][gradle-docs] is very complete and the best place to get started.

#### From command line

Like any Gradle project, use the `gradle` or `./gradlew` (not checked into SCM, you must run `gradle wrapper` first) command lines inside this project's root folder. To list all tasks available, please run: 

```shell script
$ gradle tasks
```

The most common and important tasks available are:
- `gradle build`:
    compiles, tests and packages the application
- `gradle generateContractWrappers`:
    generates Web3J wrapper (`.java`) for Solitidity (`.sol`) contracts. This may be needed to [compile the application inside your IDE](#from-your-ide).
- `gradle bootRun`:
    runs the application from command line, making it available on `localhost:8080`.

#### From your IDE

To run this project on your favorite IDE (Eclipse, IntelliJ IDEA etc) you must have the Gradle plugin for that IDE installed first. Then import this project as a Gradle Project.

You may face some compilations errors from classes that use the generated wrappers from [CryptoPunks smart contract](src/main/solidity/CryptoPunksMarket.sol). To solve these errors:
- Check whether your IDE has correctly added `build/generated/source/web3j/main/java` in the project's classpath (Eclipse) or source sets/content roots (IntelliJ IDEA). The Gradle plugin should handle this, but make sure it has done it.
- If the mentioned generated source code folder has not been created automatically, run the `generateContractWrappers` task from either your IDE or command line.
- You may also need to re-import/refresh your Gradle project after doing one or both steps above.

Once you have your code compiling, use your IDE interface to run the [main class](src/main/java/com/gitlab/rflbianco/cryptopunks/CryptoPunksApiApplication.java) as a regular Java Application.

### Governance

This project follows the [fork-based workflow][fork-workflow]. Feel free to submit any Issue, Fork this repository and open a Merge Request. When submitting any Merge Request, please be aware of our code style and guidelines. 

### Code style and Guidelines

//TODO add code style and guidelines documentation links  

[crypto-punks]: https://www.larvalabs.com/cryptopunks
[crypto-punks-details]: https://gist.github.com/maxkramer/ad0ddd1dfe8e366020b25f3ce91b9113
[ethereum]: https://ethereum.org/

[fork-workflow]: https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow

[gradle]: https://gradle.org/
[gradle-docs]: https://docs.gradle.org/
[postman]: https://getpostman.com/
