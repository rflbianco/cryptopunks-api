package com.gitlab.rflbianco.cryptopunks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptoPunksApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CryptoPunksApiApplication.class, args);
	}

}
