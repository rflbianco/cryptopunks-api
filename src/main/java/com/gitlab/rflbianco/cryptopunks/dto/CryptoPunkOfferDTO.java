package com.gitlab.rflbianco.cryptopunks.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import lombok.Data;

@Data
public class CryptoPunkOfferDTO implements Serializable {

    private BigInteger index;
    private String gender;
    private List<String> accessories;
    private BigInteger minValue;
}
