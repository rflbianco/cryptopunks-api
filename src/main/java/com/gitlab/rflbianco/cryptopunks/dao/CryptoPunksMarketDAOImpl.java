package com.gitlab.rflbianco.cryptopunks.dao;

import java.math.BigInteger;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.crypto.Wallet;
import org.web3j.crypto.WalletFile;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tuples.generated.Tuple5;

import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunkOffer;
import com.gitlab.rflbianco.cryptopunks.web3j.CryptoPunksMarket;

public class CryptoPunksMarketDAOImpl implements CryptoPunksMarketDAO {

    private static Logger LOGGER =  LoggerFactory.getLogger(CryptoPunksMarketDAOImpl.class);

    private static final String CRYPTO_PUNKS_CONTRACT = "0xb47e3cd837dDF8e4c57F05d70Ab865de6e193BBB";
    // TODO extract URL to configuration (application.properties)
    private final String infuraUrl = "https://mainnet.infura.io/v3/42488f1c6a114c24843fa98844b11945";

    private static final BigInteger GAS_PRICE = BigInteger.valueOf(1);
    private static final BigInteger GAS_LIMIT = BigInteger.valueOf(1);

    private CryptoPunksMarket cryptoPunksMarket;

    public CryptoPunksMarketDAOImpl() throws Exception {
        this.initCryptoPunksMarket();
    }

    private void initCryptoPunksMarket() throws Exception {
        Credentials credentials;
        try {
            String seed = UUID.randomUUID().toString();
            ECKeyPair exKey = null;

            exKey = Keys.createEcKeyPair();

            BigInteger privateKey = exKey.getPrivateKey();
            BigInteger publicKey = exKey.getPublicKey();

            WalletFile wallet = Wallet.createLight(seed, exKey);

            credentials = Credentials.create(Wallet.decrypt(seed, wallet));
        } catch (Exception e) {
            LOGGER.error("Error initializing Credentials to connect with CryptoPunksMarket's smart contract on Ethereum Blockchain", e);

            throw e;
        }
        String privateKeyGenerated = credentials.getEcKeyPair().getPrivateKey().toString(16);

        Web3j web3 = Web3j.build(new HttpService(this.infuraUrl));

        this.cryptoPunksMarket = CryptoPunksMarket.load(CRYPTO_PUNKS_CONTRACT, web3, credentials, GAS_PRICE, GAS_LIMIT);
    }

    @Override
    public CompletableFuture<CryptoPunkOffer> getCryptoPunkOffer(BigInteger punkIndex) {
        if (punkIndex.longValue() < 0L) {
            return CompletableFuture.completedFuture(null);
        }

        return cryptoPunksMarket.punksOfferedForSale(punkIndex).sendAsync()
                .thenApplyAsync(this::toCryptoPunkOffer);
    }

    private CryptoPunkOffer toCryptoPunkOffer(Tuple5<Boolean, BigInteger, String, BigInteger, String> tuple) {
        if (tuple == null) {
            return null;
        }

        return new CryptoPunkOffer(tuple.component2(),
                tuple.component1(),
                tuple.component3(),
                tuple.component4(),
                tuple.component5());
    }
}
