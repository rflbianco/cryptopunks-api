package com.gitlab.rflbianco.cryptopunks.dao;

import java.math.BigInteger;
import java.util.concurrent.CompletableFuture;

import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunkOffer;

/**
 * Provides a more polished/higher level data access layer to the
 * CryptoPunkMarket Smart Contract on the Ethereum blockchain.
 * <br/><br/>
 * All methods in this Interface are provided only as Async. Considering the
 * latency of connection over network necessary to communicate with the
 * blockchain, there is no reason to provide a blocking version of the methods.
 */
public interface CryptoPunksMarketDAO {
    /**
     * Asynchronously retrieve the {@link CryptoPunkOffer} of a given
     * {@code punkIndex}.
     * <br/><br/>
     * In the Smart Contract the <b>Offers</b> are stored in a {@code mapping}
     * where the key is the {@code punkIndex}. Moreover, every <b>Punk</b>
     * has a single <b>offering</b> register in the Smart Contract, whether
     * it is {@code forSale} or not. Therefore, a {@link CryptoPunkOffer} does
     * not implies that a <b>punk</b> is for sale. It is needed to evaluate
     * the {@link CryptoPunkOffer#forSale} to have that information.
     *
     * @return a {@link CompletableFuture} that shall resolve to the current
     *          offering of a <b>crypto punk</b> whether it is for sale or not.
     *          It will return {@code null} when the give {@code punkIndex} does
     *          not exist, this includes any negative numbers as all indexes are
     *          positive.
     */
    CompletableFuture<CryptoPunkOffer> getCryptoPunkOffer(BigInteger punkIndex);
}
