package com.gitlab.rflbianco.cryptopunks.restful;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.rflbianco.cryptopunks.dto.CryptoPunkOfferDTO;
import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunk;
import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunkOffer;
import com.gitlab.rflbianco.cryptopunks.service.CryptoPunksService;
import com.gitlab.rflbianco.cryptopunks.service.CryptoPunksServiceImpl;

@RestController
public class CryptoPunkController {

    Logger LOGGER = LoggerFactory.getLogger(CryptoPunkController.class);

    CryptoPunksService cryptoPunksService = new CryptoPunksServiceImpl();

    @GetMapping("/crypto-punks")
    public List<BigInteger> getCryptoPunkForSale() throws Exception {
        return this.cryptoPunksService.getCryptoPunksForSale();
    }

    @GetMapping("/crypto-punks/{punkIndex}")
    public CryptoPunkOfferDTO getCryptoPunk(@PathVariable BigInteger punkIndex) throws Exception {
        CryptoPunk cryptoPunk = this.cryptoPunksService.getCryptoPunk(punkIndex);
        CryptoPunkOffer offer = this.cryptoPunksService.getCryptoPunkOffer(punkIndex);

        CryptoPunkOfferDTO result = toCryptoPunkOfferDTO(cryptoPunk, offer);

        if (!offer.getForSale()) {
            throw new Exception("CryptoPunk not available for sale");
        }

        return result;
    }

    private CryptoPunkOfferDTO toCryptoPunkOfferDTO(CryptoPunk cryptoPunk, CryptoPunkOffer offer) throws Exception {
        if (cryptoPunk == null || offer == null) {
            throw new Exception("CrytoPunk Invalid or not available.");
        }

        if (!BigInteger.valueOf(cryptoPunk.getIndex()).equals(offer.getPunkIndex())) {
            throw new Exception(String.format("Mismatched indexes: punkIndex[%d] and offer[%d]", cryptoPunk.getIndex(), offer.getPunkIndex()));
        }

        CryptoPunkOfferDTO result = new CryptoPunkOfferDTO();

        result.setIndex(offer.getPunkIndex());
        result.setGender(cryptoPunk.getGender());
        result.setAccessories(cryptoPunk.getAccessories());
        result.setMinValue(offer.getMinValue());

        return result;
    }
}
