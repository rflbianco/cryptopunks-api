package com.gitlab.rflbianco.cryptopunks.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CryptoPunk {
    private final int index;
    private final String gender;
    private final List<String> accessories;
}
