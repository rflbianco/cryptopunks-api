package com.gitlab.rflbianco.cryptopunks.entity;

import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CryptoPunkOffer {
    private final BigInteger punkIndex;
    private final Boolean forSale;
    private final String seller;
    private final BigInteger minValue;
    private final String onlySellTo;
}
