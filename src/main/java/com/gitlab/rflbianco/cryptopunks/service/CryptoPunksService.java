package com.gitlab.rflbianco.cryptopunks.service;

import java.math.BigInteger;
import java.util.List;

import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunk;
import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunkOffer;

public interface CryptoPunksService {
    /**
     * List only the ID of all CryptoPunks that are listed for Sale
     *
     * @return List<Integer>
     */
    List<BigInteger> getCryptoPunksForSale();

    /**
     * Return a {@link CryptoPunk} based on a given {@code punkIndex}
     * when it exists, whether is it for sale or not.
     * <br/><br/>
     * Use {@link CryptoPunkOffer#getForSale()} to check whether a punk is for
     * sale.
     *
     * @param punkIndex BigInteger index of the requested <b>crypto punk</b>.
     *
     * @return The requested {@link CryptoPunk}. Returns {@code null} when the
     *          given {@code punkIndex} is not valid (i.e. out of bounds).
     */
    CryptoPunk getCryptoPunk(BigInteger punkIndex);

    /**
     * Return a {@link CryptoPunkOffer} based on a given {@code punkIndex}
     * when it exists, whether is it for sale or not.
     * <br/><br/>
     * Inspect {@link #getCryptoPunksForSale()}
     * to check whether a punk is for sale.
     *
     * @param punkIndex BigInteger index of the requested <b>crypto punk</b>.
     *
     * @return The requested {@link CryptoPunkOffer} whether the it is for sale
     *          or not. Returns {@code null} when the given {@code punkIndex} is
     *          not valid (i.e. out of bounds).
     */
    CryptoPunkOffer getCryptoPunkOffer(BigInteger punkIndex);
}
