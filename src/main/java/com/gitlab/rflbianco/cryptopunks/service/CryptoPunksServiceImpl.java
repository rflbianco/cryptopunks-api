package com.gitlab.rflbianco.cryptopunks.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rflbianco.cryptopunks.dao.CryptoPunksMarketDAO;
import com.gitlab.rflbianco.cryptopunks.dao.CryptoPunksMarketDAOImpl;
import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunk;
import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunkOffer;

public class CryptoPunksServiceImpl implements CryptoPunksService {
    private static Logger LOGGER =  LoggerFactory.getLogger(CryptoPunksServiceImpl.class);

    private static final int MIN_INDEX = 0;
    private static final int MAX_INDEX = 10000;

    private ConcurrentHashMap<BigInteger, CryptoPunk> cryptoPunksCache = new ConcurrentHashMap<>();
    private ConcurrentHashMap<BigInteger, CryptoPunkOffer> cryptoPunksOffersCache = new ConcurrentHashMap<>();

    private CryptoPunksMarketDAO cryptoPunksMarketDAO;

    public CryptoPunksServiceImpl() {
        this.init();
    }

    private void init() {
        try {
            this.cryptoPunksMarketDAO = new CryptoPunksMarketDAOImpl();
        } catch (Exception e) {
            LOGGER.error("Failure initializing the CryptoPunksMarket DAO.", e);
        }

        try {
            this.loadCryptoPunksOffersCache();
        } catch (Exception e) {
            LOGGER.error(String.format("Error loading CryptoPunksOffersCache %s. Cause: %s", this.getClass(), e.getMessage()), e);
        }

        this.loadCryptoPunksCache();
    }

    @Override
    public List<BigInteger> getCryptoPunksForSale() {
        List<BigInteger> cryptoPunksForSale = new ArrayList<>();

        for (CryptoPunkOffer offer: this.cryptoPunksOffersCache.values()) {
            if (offer.getForSale()) {
                cryptoPunksForSale.add(offer.getPunkIndex());
            }
        }

        cryptoPunksForSale.sort(BigInteger::compareTo);
        return cryptoPunksForSale;
    }

    @Override
    public CryptoPunk getCryptoPunk(BigInteger punkIndex) {
        return this.cryptoPunksCache.get(punkIndex);
    }

    @Override
    public CryptoPunkOffer getCryptoPunkOffer(BigInteger punkIndex) {
        return this.cryptoPunksOffersCache.get(punkIndex);
    }

    private void loadCryptoPunksCache() {
        Gson gson = new Gson();

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("CryptoPunks.json");
        InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(streamReader);

        Type listType = new TypeToken<ArrayList<CryptoPunk>>(){}.getType();
        List<CryptoPunk> cryptoPunks = gson.fromJson(br, listType);

        for (CryptoPunk cryptoPunk : cryptoPunks) {
            System.out.println(cryptoPunk);
            this.cryptoPunksCache.put(BigInteger.valueOf(cryptoPunk.getIndex()), cryptoPunk);
        }
    }

    private void loadCryptoPunksOffersCache() throws Exception {
        if (this.cryptoPunksMarketDAO == null) {
            throw new Exception("CryptoPunksMarket DAO not initialized.");
        }

        for (int i = MIN_INDEX; i < MAX_INDEX; i++) {
            this.cryptoPunksMarketDAO.getCryptoPunkOffer(BigInteger.valueOf(i))
                    .thenAcceptAsync(offer -> {
                        if (offer != null) {
                            LOGGER.trace("punkIndex: {}, isForSale: {}, seller: {}, minValue: {}, onlySellTo: {}",
                                    offer.getPunkIndex(),
                                    offer.getForSale(),
                                    offer.getSeller(),
                                    offer.getMinValue(),
                                    offer.getOnlySellTo());

                            this.cryptoPunksOffersCache.put(offer.getPunkIndex(), offer);
                        }
                    });
        }
    }
}
