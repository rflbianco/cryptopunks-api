package com.gitlab.rflbianco.cryptopunks.dao;

import java.math.BigInteger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class CryptoPunksMarketDAOImplTest {
    private static final int[] EXISTING_INDEXES_SAMPLE = {0, 1, 200, 1000, 199, 9999};
    private static final int[] INVALID_INDEXES_SAMPLE = {-100, -1, 10002, 20000};

    static CryptoPunksMarketDAO cryptoPunksMarketDAO;

    @BeforeAll
    static void setup() throws Exception {
        cryptoPunksMarketDAO = new CryptoPunksMarketDAOImpl();
    }

    @Test
    void getCryptoPunkOfferExists() {
        for (int sampleIndex: EXISTING_INDEXES_SAMPLE) {
            cryptoPunksMarketDAO.getCryptoPunkOffer(BigInteger.valueOf(sampleIndex))
                    .thenAccept(Assertions::assertNotNull)
                    .exceptionally(Assertions::fail);
        }
    }

    @Test
    void getCryptoPunkOfferInvalid() {
        for (int sampleIndex: INVALID_INDEXES_SAMPLE) {
            cryptoPunksMarketDAO.getCryptoPunkOffer(BigInteger.valueOf(sampleIndex))
                    .thenAccept(Assertions::assertNull)
                    .exceptionally(Assertions::fail);
        }
    }
}
