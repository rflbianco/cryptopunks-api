package com.gitlab.rflbianco.cryptopunks.service;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;

import com.gitlab.rflbianco.cryptopunks.entity.CryptoPunk;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CryptoPunksServiceImplTest {

    private static final int MAX_PUNK_INDEX = 10000;

    private static CryptoPunksService cryptoPunksService = new CryptoPunksServiceImpl();

    @Test
    void getCryptoPunksForSale() {
        List<BigInteger> punkOffers = this.cryptoPunksService.getCryptoPunksForSale();

        assertFalse(punkOffers.isEmpty());

        for (BigInteger punkOffer: punkOffers) {
            assertTrue(this.cryptoPunksService.getCryptoPunk(punkOffer) != null);
        }
    }

    @Test
    void getCryptoPunk() {
        this.cryptoPunksService.getCryptoPunk(BigInteger.valueOf(1))
                .equals(new CryptoPunk(1, "Male", Arrays.asList("Smile", "Mohawk")));

        this.cryptoPunksService.getCryptoPunk(BigInteger.valueOf(1))
                .equals(new CryptoPunk(2329, "Zombie", Arrays.asList("Peak Spike", "Earring")));

        this.cryptoPunksService.getCryptoPunk(BigInteger.valueOf(1))
                .equals(new CryptoPunk(8332, "Male", Arrays.asList("Peak Spike", "Earring", "Goat")));

        this.cryptoPunksService.getCryptoPunk(BigInteger.valueOf(1))
                .equals(new CryptoPunk(8999, "Female", Arrays.asList("Clown Eyes Green", "Blonde Short", "Purple Lipstick", "Earring")));
    }

    @Test
    void getCryptoPunkOffer() {
        for (int i = 0; i < 500; i++) {
            Random randomizer = new Random();

            Integer index = randomizer.nextInt(MAX_PUNK_INDEX);
            assertNotNull(this.cryptoPunksService.getCryptoPunkOffer(BigInteger.valueOf(index)));
        }
    }
}
